$('a[href^="#"]').click(function(){
    let anchor = $(this).attr('href');
    $('html, body').animate({
        scrollTop:  $(anchor).offset().top
    }, 600);
});


var top_show = 150;
var delay = 1000;
$(document).ready(function() {
    $(window).scroll(function () {
        if ($(this).scrollTop() > top_show) $('#top').fadeIn();
        else $('#top').fadeOut();
    });
    $('#top').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, delay);
    });
});

$(document).ready(function () {
    $(".posts").hide();
    $(".hide-show").click(function () {
        $(".posts").slideToggle("slow");

    });
});




